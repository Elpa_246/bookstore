package az.ingress.Bookstore.service.impl;

import az.ingress.Bookstore.dto.RegisterRequestDto;

public interface AuthorService {
    Long registerAuthor(RegisterRequestDto authorRegisterRequestDto);

    boolean login(String email, String password);
}
