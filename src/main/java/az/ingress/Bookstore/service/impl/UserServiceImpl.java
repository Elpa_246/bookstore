package az.ingress.Bookstore.service.impl;

import az.ingress.Bookstore.config.AppConfiguration;
import az.ingress.Bookstore.dto.RegisterRequestDto;
import az.ingress.Bookstore.model.User;
import az.ingress.Bookstore.repository.UserRepository;
import az.ingress.Bookstore.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final AppConfiguration appConfiguration;
    private final UserRepository userRepository;

    @Override
    public Long registerUser(RegisterRequestDto registerRequestDto) {
        User user = appConfiguration.getMapper().map(registerRequestDto, User.class);
        user.setActive(false);
        return userRepository.save(user).getId();
    }

    @Override
    public boolean login(String email, String password) {
        Optional<User> userOptional =userRepository.findByEmail(email);
        if (userOptional.isPresent()){
            User user =userOptional.get();
            if (password.equals(user.getPassword())&& !user.isActive()){
                user.setActive(true);
                userRepository.save(user);
                return true;
            }
        }
        return false;
    }


}
