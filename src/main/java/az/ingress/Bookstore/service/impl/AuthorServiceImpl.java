package az.ingress.Bookstore.service.impl;

import az.ingress.Bookstore.config.AppConfiguration;
import az.ingress.Bookstore.dto.RegisterRequestDto;
import az.ingress.Bookstore.model.Author;
import az.ingress.Bookstore.model.User;
import az.ingress.Bookstore.repository.AuthorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService{
    private final AuthorRepository authorRepository;
    private final AppConfiguration appConfiguration;
    @Override
    public Long registerAuthor(RegisterRequestDto authorRegisterRequestDto) {
        Author author = appConfiguration.getMapper().map(authorRegisterRequestDto, Author.class);
        author.setActive(false);
        return authorRepository.save(author).getId();
    }

    @Override
    public boolean login(String email, String password) {
        Optional<Author> userOptional =authorRepository.findByEmail(email);
        if (userOptional.isPresent()){
            Author author =userOptional.get();
            if (password.equals(author.getPassword())&& !author.isActive()){
                author.setActive(true);
                authorRepository.save(author);
                return true;
            }
        }
        return false;
    }
}
