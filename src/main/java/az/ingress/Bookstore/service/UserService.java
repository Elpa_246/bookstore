package az.ingress.Bookstore.service;

import az.ingress.Bookstore.dto.RegisterRequestDto;

public interface UserService {
    Long registerUser(RegisterRequestDto registerRequestDto);

    boolean login(String email, String password);
}
