package az.ingress.Bookstore.controller;

import az.ingress.Bookstore.dto.LoginRequestDto;
import az.ingress.Bookstore.dto.RegisterRequestDto;
import az.ingress.Bookstore.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping("/register")
    public Long registerUser(@RequestBody RegisterRequestDto registerRequestDto){
        return userService.registerUser(registerRequestDto);
    }

    @PostMapping("/login")
    public ResponseEntity <String> login(@RequestBody LoginRequestDto loginRequestDto){
        boolean login = userService.login(loginRequestDto.getEmail(), loginRequestDto.getPassword());
       if (login){
           return ResponseEntity.ok("Login successfully.");
       }else {
           return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Login unsuccessfully");
       }
    }


}
