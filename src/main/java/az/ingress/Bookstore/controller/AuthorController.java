package az.ingress.Bookstore.controller;

import az.ingress.Bookstore.dto.LoginRequestDto;
import az.ingress.Bookstore.dto.RegisterRequestDto;
import az.ingress.Bookstore.service.impl.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authors")
@RequiredArgsConstructor
public class AuthorController {
    private final AuthorService authorService;

    @PostMapping("/register")
    public Long registerAuthors(@RequestBody RegisterRequestDto authorRegisterRequestDto){
        return authorService.registerAuthor(authorRegisterRequestDto);
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody LoginRequestDto loginRequestDto){
        boolean login = authorService.login(loginRequestDto.getEmail(), loginRequestDto.getPassword());
        if (login){
            return ResponseEntity.ok("Login successfully.");
        }else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Login unsuccessfully");
        }
    }
}
