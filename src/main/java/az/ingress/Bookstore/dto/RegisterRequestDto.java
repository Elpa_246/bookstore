package az.ingress.Bookstore.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegisterRequestDto {
    String name;
    String surname;
    String email;
    String password;
    String userType;
}
