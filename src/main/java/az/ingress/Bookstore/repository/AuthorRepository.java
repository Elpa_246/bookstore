package az.ingress.Bookstore.repository;

import az.ingress.Bookstore.model.Author;
import az.ingress.Bookstore.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorRepository extends JpaRepository<Author,Long> {
    Optional<Author> findByEmail(String email);
}
